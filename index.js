// var employeeArr = [{
//         name: 'Huỳnh Minh Hiếu',
//         description: 'Huỳnh',
//         active: false

//     }, {
//         name: 'Nguyễn Tiến Linh',
//         description: 'Huh',
//         active: false

//     }, {
//         name: 'Tuấn Tờ Anh',
//         description: 'Huhaaaa',
//         active: false
//     }
// ];

var employeeArr = JSON.parse(localStorage.getItem('employeeArr'));

var formId = document.getElementById("form__id");
var formName = document.getElementById("form__name");
var formDescription = document.getElementById("form__description");

var addEmployee = document.getElementById("addEmployee");
var btnDelete = document.getElementsByClassName("btn--delete");
var btnEdit = document.getElementsByClassName("btn--edit");
var btnStar = document.getElementsByClassName("btn--star");

function renderComment() {
    var customers = document.querySelector('.js-customers');
    var content = employeeArr.map(function (item, index) {
        return `
                <tr id="${index}" class="star-${index}">
                    <td>${index + 1}</td>
                    <td>${item.name}</td> 
                    <td>${item.description}</td> 
                    <td><button class="btn--star" onclick="star(this)" id="star--${index}"> <i class="fas fa-star"></i> </button>
                    <button class="btn--delete" onclick="remove(this)" id="${index }"> Delete </button>
                    <button class="btn--edit" onclick="editEmp(this)" id="edit-${index}"> Edit </button></td>
                </tr>
            `
    })
    customers.innerHTML = content.join('');
}



var current_page = 1;
var records_per_page = 2;

var objJson = [
    { adName: "AdName 1"},
    { adName: "AdName 2"},
    { adName: "AdName 3"},
    { adName: "AdName 4"},
    { adName: "AdName 5"},
    { adName: "AdName 6"},
    { adName: "AdName 7"},
    { adName: "AdName 8"},
    { adName: "AdName 9"},
    { adName: "AdName 10"}
]; // Can be obtained from another source, such as your objJson variable

function prevPage()
{
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}

function nextPage()
{
    if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
    }
}

function changePage(page)
{
    var btn_next = document.getElementById("btn_next");
    var btn_prev = document.getElementById("btn_prev");
    var listing_table = document.getElementById("listingTable");
    var page_span = document.getElementById("page");

    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";

    for (var i = (page-1) * records_per_page; i < (page * records_per_page); i++) {
        listing_table.innerHTML += objJson[i].adName + "<br>";
    }
    page_span.innerHTML = page;

    if (page == 1) {
        btn_prev.style.visibility = "hidden";
    } else {
        btn_prev.style.visibility = "visible";
    }

    if (page == numPages()) {
        btn_next.style.visibility = "hidden";
    } else {
        btn_next.style.visibility = "visible";
    }
}

function numPages()
{
    return Math.ceil(objJson.length / records_per_page);
}

window.onload = function() {
    changePage(1);
};



function findMatches(wordToMatch, employeeArr) {
    return employeeArr.filter(place => {
        // here we need to figure out if the city or state matches what was searched
        const regex = new RegExp(wordToMatch, 'gi');
        return place.name.match(regex) || place.description.match(regex)
    });
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function displayMatches() {
    const matchArray = findMatches(this.value, employeeArr);
    const html = matchArray.map(place => {
        const regex = new RegExp(this.value, 'gi');
        const empName = place.name.replace(regex, `<span class="hl">${this.value}</span>`);
        // const empDescription = place.description.replace(regex, `<span class="hl">${this.value}</span>`);
        return `
        <li>
            <span class="name">${empName}</span>
            </li>
        `;
    }).join('');
    suggestions.innerHTML = html;
}

const searchInput = document.querySelector('.search');
const suggestions = document.querySelector('.suggestions');

searchInput.addEventListener('change', displayMatches);
searchInput.addEventListener('keyup', displayMatches);

function sortASC() {

    employeeArr.sort(function (a, b) {
        var splitA = a.name.split(" ");
        var splitB = b.name.split(" ");
        var lastA = splitA[splitA.length - 1];
        var lastB = splitB[splitB.length - 1];

        if (lastA < lastB) {
            return -1;
        }
    })

    renderComment()

}

function sortDESC() {
    employeeArr.sort(function (a, b) {
        var splitA = a.name.split(" ");
        var splitB = b.name.split(" ");
        var lastA = splitA[splitA.length - 1];
        var lastB = splitB[splitB.length - 1];

        if (lastA > lastB) {
            return -1;
        }
    })

    renderComment()
}

function star(a) {
    var starParent = document.getElementById(a.id).parentElement.parentElement;

    if (starParent.style.backgroundColor === '') {
        starParent.style.backgroundColor = 'yellow'
    } else {
        starParent.style.backgroundColor = '';
    }
}

function addEmp() {
    

    var txt;
    if (confirm("Press a button!")) {
        var emp = [{
            name: formName.value,
            description: formDescription.value
        }];

        employeeArr.push(...emp);
        localStorage.setItem('employeeArr', JSON.stringify(employeeArr));
        renderComment(employeeArr);
        e.style.display = 'none';
    } else {
        txt = "You pressed Cancel!";
    }
}

function remove(dlt) {
    var txt;

    if (confirm("Press a button!")) {
        employeeArr.splice(dlt.id, 1)
        renderComment(employeeArr);
    } else {
        txt = "You pressed Cancel!";
    }

    localStorage.setItem('employeeArr', JSON.stringify(employeeArr));

}

function eEmp() {
    var emp = {
        id: formId.value,
        name: formName.value,
        description: formDescription.value
    };

    for (let i = 0; i < employeeArr.length; i++) {
        if (emp.id == i) {
            employeeArr.splice(i, 1, emp)
        }
    }

    localStorage.setItem('employeeArr', JSON.stringify(employeeArr));

    renderComment(employeeArr);
}

function editEmp(a) {
    e.style.display = 'block';
    addEmployee.style.display = 'none' ;
    b.style.display = 'block';

    let x = document.getElementById(a.id).parentElement.parentElement;

    formId.value = x.id;
    formName.value = employeeArr[x.id].name;
    formDescription.value = employeeArr[x.id].description;

}

var e = document.getElementById('formNew');
var b = document.getElementById('eEmployee');

function toggle() {
    b.style.display = 'none';
    addEmployee.style.display = 'block' ;

    if (e.style.display == 'block' || e.style.display == '') {
        e.style.display = 'none';
    } else {
        e.style.display = 'block';
    }
}

renderComment(employeeArr);

localStorage.setItem('students', JSON.stringify(employeeArr));
localStorage.getItem(JSON.stringify(employeeArr));
